import { createApp } from 'vue';
import App from './App.vue';
import { router } from './Route';
import store from './Store';

import "@/assets/sass/black-dashboard.scss";

createApp(App).use(router).use(store).mount('#app')
