import { GetRequest, PostRequest } from '../../../Api';

const state = {
    users: [],
    user : null,
    error : null,
    pageLoader : false,
    formLoader : false  
}

const mutations = {
    setUserData (state, data) {
        state.user = data.user;
        localStorage.setItem('dash-token', data.token);
    },

    setUsersData (state, data) {
        state.users = data;
    },

    setError (state, data){
        state.error = data;
    },

    setPageLoader(state, data){
        state.pageLoader = data;
    },

     setFormLoader(state, data){
        state.formLoader = data;
    },
  
    clearUserData () {
        localStorage.removeItem('dash-token')
        state.user = null;
        location.reload();
    }
}

const actions = {
    async login ({ commit }, formData) {
        commit('setFormLoader', true);

        const response = await PostRequest(formData, 'login');

        if(response.data) commit('setUserData', response.data);

        if(response.error) commit('setError', response.error);

        commit('setFormLoader', false);

        return response;
    },

    async updateUser ({ commit }, formData) {
  
        commit('setFormLoader', true);

        const token = localStorage.getItem('dash-token');

        const response = await PostRequest(formData, `user/${formData.id}`, token);

        if(response.data) commit('setUsersData', response.data);

        if(response.error) commit('setError', response.error);

        commit('setFormLoader', false);

        return response;
    },

    async checkAuth ({ commit }) {
        commit('setPageLoader', true);

        const token = localStorage.getItem('dash-token');

        const response = await GetRequest({}, 'user', token);

        if(response.data) commit('setUserData', response.data);

        if(response.error) commit('setError', response.error);

        commit('setPageLoader', false);

        return response;

    },

    async loadUsers ({ commit } ) {
        commit('setPageLoader', true);

        const token = localStorage.getItem('dash-token');

        const response = await GetRequest({}, 'users', token);

        if(response.data) commit('setUsersData', response.data);

        if(response.error) commit('setError', response.error);

        commit('setPageLoader', false);

        return response;
    },

    async register ({ commit }, formData) {
        commit('setFormLoader', true);

        const response = await PostRequest(formData, 'register');

        if(response.data) commit('setUserData', response.data);

        if(response.error) commit('setError', response.error);

        commit('setFormLoader', false);

        return response;
    },
  
    async logout ({ commit }) {
        commit('setPageLoader', true);

        const token = localStorage.getItem('dash-token');

        const response = await PostRequest({}, 'logout', token);

        if(response.data) commit('clearUserData', response.data);

        if(response.error) commit('setError', response.error);

        commit('setPageLoader', false);

        return response;
    }
}

const getters = {
    isAuthenticated: state => !!state.user,    
    users: state => state.users,
    user: state => state.user,
    pageLoader : state => state.pageLoader,
    formLoader : state => state.formLoader
}


const AuthModule = {
    state,
    mutations,
    actions,
    getters
}

export default AuthModule;