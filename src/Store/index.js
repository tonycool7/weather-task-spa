import { createStore } from 'vuex';
import auth from './Modules/Auth';

export default createStore({
  modules: {
    auth
  }
});

