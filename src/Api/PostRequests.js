import {apiUrl} from './';
import Serialize from './Serialize';

//Handles all post requests including file uploads, Bearer token is compulsory, 
//only authenticated users can post data to api
const PostRequest = async (payload, uri, token = '') => {

  const formData = Serialize(payload);
  
  const response = await fetch(`${apiUrl}/${uri}`, {
    method: 'POST', 
    headers: {
      'Accept' : 'application/json',
      'Authorization' : 'Bearer ' + token
    },
    body: formData
  });

  return response.json();
}

export {PostRequest};