//Create form data from form elements

const serialize = (form) => {  
    const fData = new FormData();

    for(let key in form){
        fData.append(key, form[key]);
    }
  
    return fData;
}

export default serialize;