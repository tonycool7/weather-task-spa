import { GetRequest } from './GetRequests'; 
import { DeleteRequest } from './DeleteRequest'; 
import { PostRequest } from './PostRequests'; 

const apiUrl = "http://localhost:8000/v1";

export { apiUrl, GetRequest, PostRequest, DeleteRequest };