import {apiUrl} from './';

const DeleteRequest = async (uri, token = '') => {

    const response = await fetch(`${apiUrl}/${uri}`, {
        method: 'DELETE', 
        headers: {
          'Accept' : 'application/json',
          'Authorization' : 'Bearer ' + token
        }
      });

      return response.json();
}

export {DeleteRequest};