import { createWebHistory, createRouter } from "vue-router";
import { routes } from './routes';
import store from '../Store';
import axios from 'axios';

axios.interceptors.response.use(response => {
  return response;
}, error => {
   
 if (error.response.status === 401) {
   console.log("error");

  return error;
 }
});

const router = createRouter({
    mode: 'history',
    history: createWebHistory(),
    routes,
});

router.beforeEach( async (to, from, next) => {
  let user = store.getters.user;

  const token = localStorage.getItem('dash-token');

  if(!user && token) await store.dispatch('checkAuth');

  user = store.getters.user;

  if (to.matched.some(record => record.meta.requireAuth) && !user) {
    next('/login')
    return;
  }

  if (to.matched.some(record => record.meta.isAdmin) && user && !user.isAdmin) {
    alert("access denied");
    next('/')
    return;
  }

  next();

})
  
export { router };