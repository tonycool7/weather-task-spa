import DashboardLayout from "@/Modules/Layout/index.vue";

const Register = () => import('@/Modules/Auth/Register.vue');
const Login = () => import('@/Modules/Auth/Login.vue');
const Dashboard = () => import('@/Modules/Dashboard/index.vue');
const Admin = () => import('@/Modules/Admin/index.vue');

export const routes = [
    {
        path: "/",
        component: DashboardLayout,
        children: [
            {
                path : 'login',
                name : 'Login',
                component : Login,
                meta: { requireAuth: false }
            },
            {
                path : 'register',
                name : 'Register',
                component : Register,
                meta: { requireAuth: false }
            },
            {
                path : '',
                name : 'Dashboard',
                component : Dashboard,
                meta: { requireAuth: true }
            },
            {
                path : 'admin',
                name : 'Admin',
                component : Admin,
                meta: { requireAuth: true, isAdmin : true }
            },
        ]
    }
]